# building

docker build -t rsyslog .

# runing

docker run -p 514:514 -tid --name rsyslog rsyslog && docker exec -ti rsyslog /bin/bash 

# extracting instance IP

cip=`docker inspect -f '{{.NetworkSettings.IPAddress}}' rsyslog`

# testing rsyslog host instance from another machine

logger --server $cip "dupa"
logger --server $cip --port 514 --tcp "[mlm] thisis mlm message"
logger --server $cip --port 514 --tcp "[oms] thisis oms message"
